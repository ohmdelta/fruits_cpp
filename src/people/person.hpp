#ifndef PERSON_H
#define PERSON_H

#include <list>
#include "../fruits/fruits.hpp"

class Person {
    int cost;
    list<Fruit> fruit;

    public:
    Person();

    void add_cost (const int price);

    void add_fruit (Fruit& f);

    friend bool person_equals(Person person1, Person person2);
};

#endif