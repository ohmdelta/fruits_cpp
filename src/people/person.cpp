#include "person.hpp"

Person::Person() {
    cost = 0;
    fruit = {};
}

void Person::add_cost (const int price) {
    cost += price;
}

void Person::add_fruit (Fruit& f) {
    fruit.push_front(f);
} 

bool person_equals(Person person1, Person person2) {
    if (person1.cost != person2.cost)
        return false;
    else if (person1.fruit.size() != person2.fruit.size())
        return false;
    
    else {
        while (person1.fruit.size() > 0 && person2.fruit.size() > 0)
        {
            /* code */
            Fruit fruit = person1.fruit.front();

            for (auto i = person2.fruit.begin(); i != person2.fruit.end(); i++) {
                if (!fruit_equals(*i, fruit)) {
                    return false;
                };

                person2.fruit.erase(i);
                break;
            }

            person1.fruit.erase(person1.fruit.begin());
        }
        
    }
    
    return true;

}