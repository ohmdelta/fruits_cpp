#include "fruits.hpp"

Fruit::Fruit(Fruit_t f) {
    fruit = f;
    weight = 1;
}

Fruit::Fruit(Fruit_t f, int w) {
    fruit = f;
    weight = w;
}

Fruit_t Fruit::get_fruit(){
    return fruit;    
}

string Fruit::get_fruit_name() {
    return fruit_dictionary.at(fruit);
}


int Fruit::get_weight() {
    return weight;
}

bool fruit_equals(Fruit& fruit1, Fruit& fruit2) {
    return (fruit1.fruit == fruit2.fruit 
        && fruit1.weight == fruit2.weight);
}

