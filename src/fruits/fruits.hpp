#ifndef FRUITS_H
#define FRUITS_H

#include <map>
#include <string>

#include <fmt/chrono.h>

using namespace std;

enum Fruit_t {apple=1, pear=2, orange=4} ;
const map<int, string> fruit_dictionary = {{1,"apple"}, {2, "pear"},
{4,"orange"}
};

class Fruit {
    Fruit_t fruit;
    int weight;

    public:
    Fruit(Fruit_t f);

    Fruit(Fruit_t f, int w);

    Fruit_t get_fruit();

    string get_fruit_name();

    int get_weight();

    friend bool fruit_equals(Fruit& fruit1, Fruit& fruit2);

};

#endif
